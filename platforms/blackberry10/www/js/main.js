function toast(msg) {
  blackberry.ui.toast.show(msg);
}

function showLoad() {
  document.getElementById('panel').style.display = "none";
  document.getElementById('indicator').style.display = "block";
}

function hideLoad(showForm) {
  document.getElementById('indicator').style.display = "none";
  if(showForm) document.getElementById('panel').style.display = "block";
}

// function normalizeNameInputs() {
//   var name = document.getElementById("name");
//   name.onkeyup = function(e) {
//     name.value = name.value.toCapitalize();
//   }
// }

String.prototype.trim = function(){
  return this.replace(/^\s+|\s+$/g, '');
};

String.prototype.fulltrim = function() {
  return this.replace(' ', '');
};

String.prototype.toCapitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

Date.prototype.toDateInputValue = (function() {
  var local = new Date(this);
  local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
  return local.toJSON().slice(0, 16) + "Z";
});