cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.blackberry.bbm.platform/www/client.js",
        "id": "com.blackberry.bbm.platform.client",
        "clobbers": [
            "blackberry.bbm.platform"
        ]
    },
    {
        "file": "plugins/com.blackberry.ui.dialog/www/client.js",
        "id": "com.blackberry.ui.dialog.client",
        "clobbers": [
            "blackberry.ui.dialog"
        ]
    },
    {
        "file": "plugins/com.blackberry.ui.toast/www/client.js",
        "id": "com.blackberry.ui.toast.client",
        "clobbers": [
            "blackberry.ui.toast"
        ]
    },
    {
        "file": "plugins/com.blackberry.ui.contextmenu/www/client.js",
        "id": "com.blackberry.ui.contextmenu.client",
        "clobbers": [
            "blackberry.ui.contextmenu"
        ]
    },
    {
        "file": "plugins/com.blackberry.ui.cover/www/client.js",
        "id": "com.blackberry.ui.cover.client",
        "clobbers": [
            "blackberry.ui.cover"
        ]
    },
    {
        "file": "plugins/com.blackberry.app/www/client.js",
        "id": "com.blackberry.app.client",
        "clobbers": [
            "blackberry.app"
        ]
    },
    {
        "file": "plugins/com.blackberry.invoke.card/www/client.js",
        "id": "com.blackberry.invoke.card.client",
        "merges": [
            "blackberry.invoke.card"
        ]
    },
    {
        "file": "plugins/com.blackberry.io/www/client.js",
        "id": "com.blackberry.io.client",
        "merges": [
            "blackberry.io"
        ]
    }
]
});