//CREATE DB
var db = openDatabase("carlog", "1.0", "Mobile storage", (5 * 1024 * 1024));
var debug = false;

function formatDate(date) {
  var date_parts = date.split("T");

  return date_parts[0].split("-").reverse().join("/") + " " + date_parts[1].slice(0, 5);
}

function populate() {
  db.transaction(function(tx) {
    tx.executeSql("INSERT INTO fuels (title, unit, active) SELECT ?, ?, ? WHERE (select count(*) from fuels) = 0", ["Gasolina", "L", 1]);
    tx.executeSql("INSERT INTO fuels (title, unit, active) SELECT ?, ?, ? WHERE (select count(*) from fuels) = 1", ["Álcool", "L", 1]);
    tx.executeSql("INSERT INTO fuels (title, unit, active) SELECT ?, ?, ? WHERE (select count(*) from fuels) = 2", ["GNV", "m", 1]);
    tx.executeSql("INSERT INTO vehicles (title, year, picture, isdefault, active) SELECT ?, ?, ?, ?, ? WHERE (select count(*) from vehicles) = 0", ["Celta", 2012, "", 0, 1]);
    tx.executeSql("INSERT INTO vehicles (title, year, picture, isdefault, active) SELECT ?, ?, ?, ?, ? WHERE (select count(*) from vehicles) = 1", ["Fiesta", 2013, "", 1, 1]);
    tx.executeSql("INSERT INTO vehicles (title, year, picture, isdefault, active) SELECT ?, ?, ?, ?, ? WHERE (select count(*) from vehicles) = 2", ["Kombi", 1986, "", 0, 1]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 0", [1, 1]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 1", [1, 2]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 2", [2, 1]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 3", [2, 2]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 4", [3, 1]);
    tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) SELECT ?, ? WHERE (select count(*) from vehicle_fuel) = 5", [3, 3]);
  });
}

// load method
function loadFills() {
  db.transaction(function (tx) {
    tx.executeSql("CREATE TABLE IF NOT EXISTS fuels (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(50) NOT NULL, unit VARCHAR(5) NOT NULL, active BOOLEAN)", [], function (tx) {
      tx.executeSql("CREATE TABLE IF NOT EXISTS vehicles (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(100) NOT NULL, year INTEGER(4), picture VARCHAR(200), isdefault BOOLEAN NOT NULL, active BOOLEAN)", [], function (tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS settings (distance_unit VARCHAR(2) NOT NULL, currency VARCHAR(50), first_screen VARCHAR(10))", [], function (tx) {
          tx.executeSql("INSERT INTO settings (distance_unit, currency, first_screen) SELECT 'mi', 'USD', 'list' WHERE (select count(*) from settings) = 0", [], function(tx, results) {
            if (results.rowsAffected == 1) {
              localStorage.setItem("distance_unit", "mi");
              localStorage.setItem("currency", "USD");
              localStorage.setItem("first_screen", "list");
            }
            tx.executeSql("CREATE TABLE IF NOT EXISTS vehicle_fuel (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vehicle_id INTEGER NOT NULL, fuel_id INTEGER NOT NULL)", [], function (tx) {
              tx.executeSql("CREATE TABLE IF NOT EXISTS fills (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, vehicle_id INTEGER NOT NULL, fuel_id INTEGER, price REAL NOT NULL, odometer INTEGER NOT NULL, diff INTEGER, quantity REAL NOT NULL, paid REAL NOT NULL, full BOOLEAN NOT NULL, date DATETIME NOT NULL, consumption REAL)", [], function (tx) {
                tx.executeSql("CREATE INDEX IF NOT EXISTS idx_vehicle_fuel_vehicle_id ON vehicle_fuel (vehicle_id)", [], function (tx) {
                  tx.executeSql("CREATE INDEX IF NOT EXISTS idx_vehicle_fuel_fuel_id ON vehicle_fuel (fuel_id)", [], function (tx) {
                    if (debug)  {
                      populate();
                    }
                    tx.executeSql("SELECT f.*, v.title AS vehicle, fu.title AS fuel, fu.unit FROM fills f INNER JOIN vehicles v ON f.vehicle_id = v.id INNER JOIN fuels fu ON f.fuel_id = fu.id ORDER BY v.title, fu.title, f.date DESC", [], function (tx, results) {
                      var dataList = document.getElementById("dataList");
                      dataList.clear();
                      var count = results.rows.length;
                      if (count > 0) {
                        var items = [],
                            oldVehicle = "",
                            oldFuel = "";
                        
                        for (var i = 0; i < count; i++) {
                          var row = results.rows.item(i),
                              item = document.createElement("div"),
                              vehicle = row.vehicle,
                              fuel = row.fuel;
                          
                          if (vehicle !== oldVehicle || fuel !== oldFuel) {
                            var header = document.createElement("div");
                            header.setAttribute("data-bb-type", "header");
                            header.innerHTML = vehicle + " - " + fuel;
                            items.push(header);

                            oldVehicle = vehicle;
                            oldFuel = fuel;
                          }

                          item.setAttribute("data-bb-type", "item");
                          item.setAttribute("data-bb-title", row.quantity + row.unit +  " / " + row.paid + " " + localStorage.getItem("currency") + " (" + row.price + ")");
                          item.setAttribute("id", row.id);
                          if (row.consumption === -1) {
                            item.setAttribute("data-bb-accent-text", "First fillup");
                          } else if (!row.full && row.consumption !== -1) {
                            item.setAttribute("data-bb-accent-text", "Partial");
                          } else {
                            item.setAttribute("data-bb-accent-text", row.consumption + " " + localStorage.getItem("distance_unit") + "/" + row.unit);
                          }
                          var html = formatDate(row.date);
                          if (row.diff) {
                            html += " / Diff: " + row.diff + " " + localStorage.getItem("distance_unit");
                          }
                          item.innerHTML = html;

                          items.push(item);
                        }

                        dataList.refresh(items);

                        if (bb.scroller) {
                          bb.scroller.refresh();
                        }
                      } else {
                        document.getElementById("fillsNotFound").style.display = "block";
                      }
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

function loadVehicles() {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM vehicles WHERE active = 1 ORDER BY title COLLATE NOCASE", [], function (tx, results) {
      var dataList = document.getElementById("dataList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [];
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              item = document.createElement("div");

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.title);
          if (row.picture) {
            item.setAttribute('data-bb-img', row.picture);
          } else {
            item.setAttribute('data-bb-img', 'local:///img/icons/vehicles.png');
          }
          item.setAttribute("id", row.id);
          if (row.year) {
            item.innerHTML = "Year: " + row.year;
          }
          if(row.isdefault) {
            item.setAttribute("data-bb-accent-text", "Default");
          }

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("vehiclesNotFound").style.display = "block";
      }
    });
  });
}

function loadFuels() {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fuels WHERE active = 1 ORDER BY title COLLATE NOCASE", [], function (tx, results) {
      var dataList = document.getElementById("dataList");
      dataList.clear();
      var count = results.rows.length;
      if (count > 0) {
        var items = [];
        for (var i = 0; i < count; i++) {
          var row = results.rows.item(i),
              item = document.createElement("div");

          item.setAttribute("data-bb-type", "item");
          item.setAttribute("data-bb-title", row.title);
          item.setAttribute("id", row.id);
          item.innerHTML = "Unit: " + row.unit;
          item.onclick = function () {
            bb.pushScreen("editFuel.html", "editFuel", dataList.selected);
          };

          items.push(item);
        }

        dataList.refresh(items);

        if (bb.scroller) {
          bb.scroller.refresh();
        }
      } else {
        document.getElementById("fuelsNotFound").style.display = "block";
      }
    });
  });
}

function loadFuelsDrop(vehicle_id, fuel_id) {
  var dropFuel = document.getElementById('fuel');
  dropFuel.innerHTML = "";
  
  db.transaction(function (tx) {
    tx.executeSql("SELECT f.id, f.title FROM vehicle_fuel vf INNER JOIN fuels f ON vf.fuel_id = f.id AND f.active = 1 WHERE vf.vehicle_id = ? ORDER BY f.title COLLATE NOCASE", [vehicle_id], function (tx, results) {
      var count = results.rows.length;

      for (var i = 0; i < count; i++) {
        var opt = document.createElement('option'),
            row = results.rows.item(i);

        opt.setAttribute('value', row.id);
        opt.innerHTML = row.title;

        if (fuel_id == row.id) {
          opt.setAttribute('selected', true);
        }

        dropFuel.appendChild(opt);
      }

      dropFuel.refresh();
      dropFuel.enable();
    });
  });
}

function newFill() {
  document.getElementById("date").value = new Date().toDateInputValue();
}

function loadFillDependencies(vehicle_id, fuel_id) {
  showLoad();

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM vehicles WHERE active = 1 ORDER BY title COLLATE NOCASE", [], function (tx, results) {
      var count = results.rows.length;

      //if (count > 0) {
        var dropVehicle = document.getElementById('vehicle'),
            dropFuel = document.getElementById('fuel');

        for (var i = 0; i < count; i++) {
          var option = document.createElement('option'),
              row = results.rows.item(i);

          option.setAttribute('value', row.id);
          option.innerHTML = row.title;

          if (row.isdefault && !vehicle_id) {
            option.setAttribute('selected', true);
            loadFuelsDrop(row.id);
          }

          if (vehicle_id == row.id) {
            option.setAttribute('selected', true);
            loadFuelsDrop(row.id, fuel_id);
          }

          dropVehicle.appendChild(option);
        }

        dropVehicle.onchange = function() {
          loadFuelsDrop(this.value);
        };

        dropVehicle.refresh();
        bb.refresh();
        hideLoad(true);
      // } else {
      //   document.getElementById("vehiclesNotFound").style.display = "block";
      //   hideLoad(false);
      // }
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function vehicleFormShowVehicle() {
  document.getElementById("vehicle").style.display = "block";
  document.getElementById("fuels").style.display = "none";
}

function vehicleFormShowFuels() {
  document.getElementById("vehicle").style.display = "none";
  document.getElementById("fuels").style.display = "block";
}

function invokeFilePicker(details) {
  blackberry.io.sandbox = false;
  blackberry.invoke.card.invokeFilePicker(details,
    function (path) {
      var file = "file://" + path;
      document.getElementById("path").value = file;
      document.getElementById("panel-picture").style.backgroundImage = "url('" + file + "')";
    }
  );
}

//pick a file of picture with image crop enabled
function invokeFileInPicker() {
  var details = {mode: blackberry.invoke.card.FILEPICKER_MODE_PICKER,
                 type: [blackberry.invoke.card.FILEPICKER_TYPE_PICTURE]};
  invokeFilePicker(details);
}

function loadVehicleDependencies() {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fuels WHERE active = 1 ORDER BY title COLLATE NOCASE", [], function (tx, results) {
      var count = results.rows.length;
      if (count) {
        var table = document.getElementById("fuels-table");
        for(var i = 0; i < count; i++) {
          var item = results.rows.item(i),
              row = document.createElement("tr"),
              label = document.createElement("td"),
              container = document.createElement("td"),
              checkbox = document.createElement("input");

          label.className = "cell";
          label.style.paddingTop = "7px";
          label.innerHTML = item.title;

          checkbox.className = "fuelsCheckbox";
          checkbox.setAttribute("type", "checkbox");
          checkbox.setAttribute("value", item.id);
          checkbox = bb.checkbox.style(checkbox);

          container.className = "cell";
          container.appendChild(checkbox);

          row.appendChild(label);
          row.appendChild(container);
          table.appendChild(row);
        }
        bb.refresh();
      } else {
        document.getElementById("fuelsNotFound").style.display = "block";
      }
    });
  });
}

function addFill() {
  showLoad();

  var vehicle = document.getElementById("vehicle").value,
      fuel = document.getElementById("fuel").value,
      date = document.getElementById("date").value,
      paid = document.getElementById("paid").value.trim(),
      odometer = document.getElementById("odometer").value.trim(),
      quantity = document.getElementById("quantity").value.trim(),
      full = document.getElementById("full").getChecked(),
      isFull = full ? 1 : 0,
      price = (paid / quantity).toFixed(2);

  if (vehicle === "") {
    hideLoad(true);
    toast("Vehicle required.");
    return;
  }

  if (fuel === "") {
    hideLoad(true);
    toast("Fuel required.");
    return;
  }

  if (paid === "") {
    hideLoad(true);
    toast("Paid required.");
    return;
  }

  if (odometer === "") {
    hideLoad(true);
    toast("Odometer required.");
    return;
  }

  if (quantity === "") {
    hideLoad(true);
    toast("Quantity required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fills WHERE vehicle_id = ? AND fuel_id = ? ORDER BY id DESC LIMIT 1", [vehicle, fuel], function(tx, results) {
      var diff = 0,
          consumption = 0;

      if (results.rows.length > 0) {
        var prev = results.rows.item(0);

        if (prev.odometer >= odometer) {
          hideLoad(true);
          toast("The odometer must be greater than the previous (" + prev.odometer + " " + localStorage.getItem("distance_unit") + ").");
          return;
        }

        diff = odometer - prev.odometer;

        if (full) {
          consumption = (diff/quantity).toFixed(2);
        }
      } else {
        consumption = -1;
      }

      tx.executeSql("INSERT INTO fills (vehicle_id, fuel_id, price, odometer, diff, quantity, paid, full, date, consumption) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [vehicle, fuel, price, odometer, diff, quantity, paid, isFull, date, consumption], function (tx, results) {
        hideLoad(false);
        toast("Fill added successfully.");
        bb.popScreen();
      }, function (tx, error) {
        hideLoad(true);
        toast("Ops... " + error.message);
      });
    });
  });
}

function addVehicle() {
  showLoad();

  var title = document.getElementById("title").value.trim().toCapitalize(),
      year = document.getElementById("year").value.trim(),
      isDefault = document.getElementById("isdefault").getChecked(),
      isDefaultValue = isDefault ? 1 : 0,
      picture = document.getElementById("path").value;

  if (title === "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  db.transaction(function (tx) {
    if (isDefault) {
      tx.executeSql("UPDATE vehicles SET isdefault = 0");
    }

    tx.executeSql("INSERT INTO vehicles (title, year, picture, isdefault, active) VALUES (?, ?, ?, ?, ?)", [title, year, picture, isDefaultValue, 1], function (tx, results) {
      var vehicle_id = results.insertId,
          fuels = document.getElementsByClassName("fuelsCheckbox");

      for(var i = 0; i < fuels.length; i++) {
        if (fuels[i].getChecked()) {
          tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) VALUES (?, ?)", [vehicle_id, fuels[i].value]);
        }
      }

      hideLoad(false);
      toast("Vehicle added successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function addFuel() {
  showLoad();

  var title = document.getElementById("title").value.trim().toCapitalize(),
      unit = document.getElementById("unit").value.trim();

  if (title === "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  if (unit === "") {
    hideLoad(true);
    toast("Unit required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("INSERT INTO fuels (title, unit, active) VALUES (?, ?, ?)", [title, unit, 1], function (tx, results) {
      hideLoad(false);
      toast("Fuel added successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function editFill(ev) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fills WHERE id = ?", [ev.id], function (tx, results) {
      var row = results.rows.item(0);
      loadFillDependencies(row.vehicle_id, row.fuel_id);
      document.getElementById("id").value = row.id;
      document.getElementById("date").value = row.date;
      document.getElementById("paid").value = row.paid;
      document.getElementById("odometer").value = row.odometer;
      document.getElementById("quantity").value = row.quantity;
      if (row.full) {
        document.getElementById("full").setChecked(true);
      }
    });
  });
}

function editVehicle(ev) {
  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM vehicles WHERE id = ?", [ev.id], function (tx, results) {
      var row = results.rows.item(0);
      document.getElementById("id").value = row.id;
      document.getElementById("title").value = row.title;
      document.getElementById("year").value = row.year;
      if (row.picture) {
        document.getElementById("path").value = row.picture;
        document.getElementById("panel-picture").style.backgroundImage = "url('" + row.picture + "')";
      }
      if (row.isdefault) {
        document.getElementById("isdefault").setChecked(true);
      }

      // load fuels
      tx.executeSql("SELECT f.*, vf.fuel_id FROM fuels f LEFT JOIN vehicle_fuel vf ON f.id = vf.fuel_id AND vf.vehicle_id = ? AND f.active = 1 ORDER BY f.title COLLATE NOCASE", [row.id], function (tx, results) {
        var count = results.rows.length;
        if (count) {
          var table = document.getElementById("fuels-table");
          for(var i = 0; i < count; i++) {
            var item = results.rows.item(i),
                row = document.createElement("tr"),
                label = document.createElement("td"),
                container = document.createElement("td"),
                checkbox = document.createElement("input");

            label.className = "cell";
            label.style.paddingTop = "7px";
            label.innerHTML = item.title;

            checkbox.className = "fuelsCheckbox";
            checkbox.setAttribute("type", "checkbox");
            checkbox.setAttribute("value", item.id);
            if (item.fuel_id) {
              checkbox.checked = true;
            }
            checkbox = bb.checkbox.style(checkbox);

            container.className = "cell";
            container.appendChild(checkbox);

            row.appendChild(label);
            row.appendChild(container);
            table.appendChild(row);
          }
          bb.refresh();
        } else {
          document.getElementById("fuelsNotFound").style.display = "block";
        }
      });
      
    }, function (tx, error) {
      toast("Ops... " + error.message);
    });
  });
}

function editFuel(ev) {
  showLoad();

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fuels WHERE id = ?", [ev.id], function (tx, results) {
      var row = results.rows.item(0);
      document.getElementById("id").value = row.id;
      document.getElementById("title").value = row.title;
      document.getElementById("unit").value = row.unit;
      
      hideLoad(true);
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function updateFill() {
  showLoad();

  var id = document.getElementById("id").value,
      vehicle = document.getElementById("vehicle").value,
      fuel = document.getElementById("fuel").value,
      date = document.getElementById("date").value,
      paid = document.getElementById("paid").value.trim(),
      odometer = document.getElementById("odometer").value.trim(),
      quantity = document.getElementById("quantity").value.trim(),
      full = document.getElementById("full").getChecked(),
      isFull = full ? 1 : 0,
      price = (paid / quantity).toFixed(2);

  if (vehicle === "") {
    hideLoad(true);
    toast("Vehicle required.");
    return;
  }

  if (fuel === "") {
    hideLoad(true);
    toast("Fuel required.");
    return;
  }

  if (paid === "") {
    hideLoad(true);
    toast("Paid required.");
    return;
  }

  if (odometer === "") {
    hideLoad(true);
    toast("Odometer required.");
    return;
  }

  if (quantity === "") {
    hideLoad(true);
    toast("Quantity required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM fills WHERE vehicle_id = ? AND fuel_id = ? AND id != ? ORDER BY id DESC LIMIT 1", [vehicle, fuel, id], function(tx, results) {
      var diff = 0,
          consumption = 0;

      if (results.rows.length > 0) {
        var prev = results.rows.item(0);

        if (prev.odometer >= odometer) {
          hideLoad(true);
          toast("The odometer must be greater than the previous (" + prev.odometer + " " + localStorage.getItem("distance_unit") + ").");
          return;
        }

        diff = odometer - prev.odometer;

        if (full) {
          consumption = (diff/quantity).toFixed(2);
        }
      } else {
        consumption = -1;
      }

      tx.executeSql("UPDATE fills SET vehicle_id = ?, fuel_id = ?, price = ?, odometer = ?, diff = ?, quantity = ?, paid = ?, full = ?, date = ?, consumption = ? WHERE id = ?", [vehicle, fuel, price, odometer, diff, quantity, paid, isFull, date, consumption, id], function (tx, results) {
        hideLoad(false);
        toast("Fill updated successfully.");
        bb.popScreen();
      }, function (tx, error) {
        hideLoad(true);
        toast("Ops... " + error.message);
      });
    });
  });
}

function updateVehicle() {
  showLoad();

  var id = document.getElementById("id").value,
      title = document.getElementById("title").value.trim().toCapitalize(),
      year = document.getElementById("year").value.trim(),
      isDefault = document.getElementById("isdefault").getChecked(),
      isDefaultValue = isDefault ? 1 : 0,
      picture = document.getElementById("path").value;

  if (title === "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  db.transaction(function (tx) {
    if (isDefault) {
      tx.executeSql("UPDATE vehicles SET isdefault = 0");
    }

    tx.executeSql("UPDATE vehicles SET title = ?, year = ?, picture = ?, isdefault = ? WHERE id = ?", [title, year, picture, isDefaultValue, id], function (tx, results) {
      var fuels = document.getElementsByClassName("fuelsCheckbox");
      tx.executeSql("DELETE FROM vehicle_fuel WHERE vehicle_id = ?", [id]);

      for(var i = 0; i < fuels.length; i++) {
        if (fuels[i].getChecked()) {
          tx.executeSql("INSERT INTO vehicle_fuel (vehicle_id, fuel_id) VALUES (?, ?)", [id, fuels[i].value]);
        }
      }

      hideLoad(false);
      toast("Vehicle updated successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function updateFuel() {
  showLoad();

  var id = document.getElementById("id").value,
      title = document.getElementById("title").value.trim().toCapitalize(),
      unit = document.getElementById("unit").value.trim();

  if (title === "") {
    hideLoad(true);
    toast("Title required.");
    return;
  }

  db.transaction(function (tx) {
    tx.executeSql("UPDATE fuels SET title = ?, unit = ? WHERE id = ?", [title, unit, id], function (tx, results) {
      hideLoad(false);

      toast("Fuel updated successfully.");
      bb.popScreen();
    }, function (tx, error) {
      hideLoad(true);
      toast("Ops... " + error.message);
    });
  });
}

function removeFill(item) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected fill", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM fills WHERE id = ?", [id], function (tx) {
          loadFills();
          toast("Fill deleted successfully.");
        });
      });
    }
  }, {title: "Delete?"});
}

function removeVehicle(item) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected vehicle", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM vehicle_fuel WHERE vehicle_id = ?", [id], function (tx) {
          tx.executeSql("UPDATE vehicles SET active = ? WHERE id = ?", [0, id], function (tx) {
            loadVehicles();
            toast("Vehicle deleted successfully.");
          });
        });
      });
    }
  }, {title: "Delete?"});
}

function removeFuel(item) {
  var id = item.id;
  blackberry.ui.dialog.customAskAsync("This will delete the selected fuel", ["Cancel", "Delete"], function (idx) {
    if (idx === 1) {
      db.transaction(function (tx) {
        tx.executeSql("DELETE FROM vehicle_fuel WHERE fuel_id = ?", [id], function (tx) {
          tx.executeSql("UPDATE fuels SET active = ? WHERE id = ?", [0, id], function (tx) {
            loadFuels();
            toast("Fuel deleted successfully.");
          });
        });
      });
    }
  }, {title: "Delete?"});
}

function mountOption(obj, i, variable) {
  var option = document.createElement('option'),
      key = Object.keys(obj)[i];

  option.setAttribute('value', key);
  option.innerHTML = obj[key];

  if (key === variable) {
    option.setAttribute('selected', true);
  }

  return option;
}

function loadSettings() {
  var distance_unit = document.getElementById("distance_unit"),
      currency = document.getElementById("currency"),
      first_screen = document.getElementById("first_screen");

  db.transaction(function (tx) {
    tx.executeSql("SELECT * FROM settings", [], function (tx, results) {
      row = results.rows.item(0);

      for (var i = 0; i < Object.keys(distance_units).length; i++) {
        var option = mountOption(distance_units, i, row.distance_unit);
        distance_unit.appendChild(option);
      }

      for (var i = 0; i < Object.keys(currencies).length; i++) {
        var option = mountOption(currencies, i, row.currency);
        currency.appendChild(option);
      }

      for (var i = 0; i < Object.keys(first_screens).length; i++) {
        var option = mountOption(first_screens, i, row.first_screen);
        first_screen.appendChild(option);
      }

      distance_unit.refresh();
      currency.refresh();
      first_screen.refresh();
      bb.refresh();
    }, function (tx, error) {
      toast("Ops... " + error.message);
    });
  });
}

function saveSettings() {
  var distance_unit = document.getElementById("distance_unit").value,
      currency = document.getElementById("currency").value,
      first_screen = document.getElementById("first_screen").value;

  localStorage.setItem("distance_unit", distance_unit);
  localStorage.setItem("currency", currency);
  localStorage.setItem("first_screen", first_screen);

  db.transaction(function (tx) {
    tx.executeSql("UPDATE settings SET distance_unit = ?, currency = ?, first_screen = ?", [distance_unit, currency, first_screen], function (tx, results) {
    }, function (tx, error) {
      toast("Ops... " + error.message);
    });
  });
}

var distance_units = {"MI": "Miles (mi)", "KM": "Kilometers (km)"};
var first_screens = {"new": "New Fill", "list": "Fills List"};
var currencies = {"USD": "USD - US Dollar",
"GBP": "GBP - British Pound",
"INR": "INR - Indian Rupee",
"AUD": "AUD - Australian Dollar",
"CAD": "CAD - Canadian Dollar",
"AED": "AED - Emirati Dirham",
"AED": "AED - Emirati Dirham",
"ARS": "ARS - Argentine Peso",
"AUD": "AUD - Australian Dollar",
"BGN": "BGN - Bulgarian Lev",
"BHD": "BHD - Bahraini Dinar",
"BND": "BND - Bruneian Dollar",
"BRL": "BRL - Brazilian Real",
"BWP": "BWP - Botswana Pula",
"CAD": "CAD - Canadian Dollar",
"CHF": "CHF - Swiss Franc",
"CLP": "CLP - Chilean Peso",
"CNY": "CNY - Chinese Yuan Renminbi",
"COP": "COP - Colombian Peso",
"CZK": "CZK - Czech Koruna",
"DKK": "DKK - Danish Krone",
"EUR": "EUR - Euro",
"GBP": "GBP - British Pound",
"HKD": "HKD - Hong Kong Dollar",
"HRK": "HRK - Croatian Kuna",
"HUF": "HUF - Hungarian Forint",
"IDR": "IDR - Indonesian Rupiah",
"ILS": "ILS - Israeli Shekel",
"INR": "INR - Indian Rupee",
"IRR": "IRR - Iranian Rial",
"ISK": "ISK - Icelandic Krona",
"JPY": "JPY - Japanese Yen",
"KRW": "KRW - South Korean Won",
"KWD": "KWD - Kuwaiti Dinar",
"KZT": "KZT - Kazakhstani Tenge",
"LKR": "LKR - Sri Lankan Rupee",
"LTL": "LTL - Lithuanian Litas",
"LVL": "LVL - Latvian Lat",
"LYD": "LYD - Libyan Dinar",
"MUR": "MUR - Mauritian Rupee",
"MXN": "MXN - Mexican Peso",
"MYR": "MYR - Malaysian Ringgit",
"NOK": "NOK - Norwegian Krone",
"NPR": "NPR - Nepalese Rupee",
"NZD": "NZD - New Zealand Dollar",
"OMR": "OMR - Omani Rial",
"PHP": "PHP - Philippine Peso",
"PKR": "PKR - Pakistani Rupee",
"PLN": "PLN - Polish Zloty",
"QAR": "QAR - Qatari Riyal",
"RON": "RON - Romanian New Leu",
"RUB": "RUB - Russian Ruble",
"SAR": "SAR - Saudi Arabian Riyal",
"SEK": "SEK - Swedish Krona",
"SGD": "SGD - Singapore Dollar",
"THB": "THB - Thai Baht",
"TRY": "TRY - Turkish Lira",
"TTD": "TTD - Trinidadian Dollar",
"TWD": "TWD - Taiwan New Dollar",
"USD": "USD - US Dollar",
"VEF": "VEF - Venezuelan Bolivar",
"ZAR": "ZAR - South African Rand"};